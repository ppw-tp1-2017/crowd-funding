from django.urls import path
from .views import *

app_name = 'about'

urlpatterns = [
    path('about/', index),
    path('post/',post,name='post'),
    # path('about/get_data/',get_data,name='get_data')
    # path('post_testi/',post_testi, name='post_testi'),
]