from django.db import models

# Create your models here.
class Testimoni(models.Model):
    sender = models.TextField(max_length=50)
    testi = models.TextField(max_length=200)
