from django.shortcuts import render,reverse, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from .models import *
from .forms import TestiForm
import json

def index(request):
 response = {
  'form' : TestiForm(),
  'testies' : Testimoni.objects.all().values()
 }
 html = 'about.html'
 return render (request, html, response)

# def post_testi(request):
# 	if 'user_login' in request.session:
# 		posted = Testimoni.objects.all()
# 		if (request.method == 'POST'):
# 			testii = Testimoni(
# 			sender = request.user.username,
# 			testi = request.POST['testi']
# 			)
# 			testii.save()

def post(request):
    
    if request.is_ajax() and request.method == "POST":
        data = json.loads(request.body)
        form = TestiForm({'testi' : data['testi']})
        if form.is_valid():
            model = Testimoni(sender = request.user.get_full_name(), testi = data['testi'])
            model.save()

            all_data = Testimoni.objects.all().values()
            testimonies = list(all_data)
            return JsonResponse({'testimonies' : testimonies})
            # return HttpResponse('about.html')
	
	
# def get_data(request):
# 	all_data = Testimoni.objects.all().values()
# 	testimonies = list(all_data)
# 	return JsonResponse(testimonies, safe=False)

# Create your views here.
# def about(request):
#     return render(request,'about/about.html')

# data_ready = {}
# def about(request):
#  testiForm = TestiForm()
#   data = list(Testimoni.objects.all())
#  data.reverse()
#  data_ready['data'] = data
#  return render(request, 'about.html', data_ready)

# def add_testi(request):
#  if request.method == 'POST':
#   testi = request.POST['testi']
#   sender = request.user.username
#   Testimony.objects.create(sender=sender, testi=testi)
#   return JsonResponse({'saved': True, 'sender': sender, 'testi': testi})
#  return JsonResponse({'saved': False})

#
# def add_testi(request):
#  if request.method == 'POST':
#   testiForm = TestiForm(request.POST)
#   if form.is_valid():
#    new_testi = Testimoni()
#    new_testi.testi = form.cleaned_data['testi']
#    new_testi.sender = request.user.username  
#    new_testi.save()
#    return redirect('about:about')
#  else:
#   form = TestiForm()
#  return render(request, 'about.html', {'form':form, 'data':data})