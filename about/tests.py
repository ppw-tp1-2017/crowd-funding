from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *

import time
import unittest


# Create your tests here.
class AboutTest(TestCase):
    def test_about_url_is_exist(self):
        response = Client().get('/kitasabi/about/')
        self.assertEqual(response.status_code,200)

    def test_about_using_about_funct(self):
        found = resolve('/kitasabi/about/')
        self.assertEqual(found.func, index)
    
    def test_about_html_is_exist(self):
        response = Client().get('/kitasabi/about/')
        self.assertTemplateUsed(response, 'about.html')

    # def test_testi_model_can_create_new_testi(self):
    #     new_testi = Testimoni.objects.create(sender='THETDDMASTER',testi='Thankyou very much, KitaSabi.com by SinarPerak. It really helps me doing my angelic stuffs. Love ya')

    #     conting_all_testi = Testimony.objects.all().count()
    #     self.assertEqual(counting_all_testi,1)
        
    # def test_model_can_create_new_activity(self):
    #         #Creating a new activity
    #         new_activity = Diary.objects.create(date=timezone.now(),activity='Aku mau latihan ngoding deh')

    #         #Retrieving all available activity
    #         counting_all_available_activity = Diary.objects.all().count()
    #         self.assertEqual(counting_all_available_activity,1)
