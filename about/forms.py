from django import forms
from .models import *

class TestiForm(forms.Form):
    testi = forms.CharField(widget=forms.Textarea(
        attrs={
            'placeholder':'share people what you think about us',
            'required': True,
        }
    ))
