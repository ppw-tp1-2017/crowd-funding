from django.db import models

# Create your models here.
class Program(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=300)
    images = models.CharField(max_length=100)
    #linkimages = models.CharField(max_length=70)

    def __str__(self):
        return self.title    