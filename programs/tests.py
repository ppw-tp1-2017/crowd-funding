from django.test import TestCase, Client
from django.urls import resolve
from .views import programs
from .models import Program

# Create your tests here.
class ProgramsTest(TestCase):
    def test_programs_url_is_exist(self):
        response = Client().get('/kitasabi/programs/')
        self.assertEqual(response.status_code,200)

    def test_programs_using_programs_funct(self):
        found = resolve('/kitasabi/programs/')
        self.assertEqual(found.func,programs)
    
    def test_html_is_exist(self):
        response = Client().get('/kitasabi/programs/')
        self.assertTemplateUsed(response, 'programs.html')

    '''def test_model_created_successfully(self):
        mamat = Program.objects.create('kintan','yuyu','google.com')
    
        counting_all_object = Program.objects.all().count()
        self.assertEqual(counting_all_available,1)'''