from django.shortcuts import render
from .models import Program

# Create your views here.
def programs(request):
    programs = Program.objects.all()
    return render(request, 'programs.html', {'content_programs':programs})