from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from .views import *

app_name = 'home'

urlpatterns = [
    path('', index, name = 'index'),
    path('redirect-news', news, name = 'news' ),
    path('redirect-programs', programs, name= 'programs'),
    path('redirect-donate', donate, name = "donate"),
    path('redirect-signup', signup, name = "signup"),
    path('redirect-about', about, name='about'),
    path('admin/', admin.site.urls),
    
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    # url(r'^$', home, name='home'),
]
