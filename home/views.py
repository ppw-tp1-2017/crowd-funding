from django.shortcuts import render, redirect
from forms.models import User

def index(req):
    if req.user.is_authenticated:
        try:
            user = User.objects.get(email = req.user.email)
        except:
            create_user_obj(req)

    return render(req, 'home/index.html',)

def news(req):
	return redirect('news/')
def programs(req):
	return redirect('programs/')
def donate(req):
	return redirect('donate/')
def signup(req):
    return redirect('signup/')	
def about(req):
    return redirect('about/')

# def logout(request):
#     request.session.flush()
# 	return render(request, 'index.html')

def create_user_obj(req):
    new_user = User()

    new_user.email = req.user.email
    new_user.name = req.user.username

    new_user.save()



