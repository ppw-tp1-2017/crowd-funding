from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class HomePageUnitTest(TestCase):
    def test_url_is_exist(self):
        response = self.client.get('/kitasabi/')
        self.assertEqual(response.status_code,200)

    def test_url_not_found(self):
        response = self.client.get('/not_exist')
        self.assertEqual(response.status_code,404)

    def test_using_index_func(self):
        found = resolve('/kitasabi/')
        self.assertEqual(found.func, index)

    def test_using_proper_template(self):
        response =self.client.get('/kitasabi/')
        self.assertTemplateUsed(response, 'home/index.html')

    # def test_home_contains_content(self):
    #     response = self.client.get('/kitasabi/')
    #     self.assertIn('Hello World', response.content.decode('utf8'))