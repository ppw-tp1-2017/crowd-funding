$(document).ready(function() {
    requestDonations();
});

function requestDonations() {
    $.ajax({
        url: ('https://tp-ppw-crowdfunding.herokuapp.com/kitasabi/userprofile/'),
        success: function (response) {
            if (response.is_auth) {
                buildTable(response.donations);
            } else {
                $("#main").html("<h4>Not logged in!</h4>");
            }
        }
    })
}

function buildTable(data) {
    const tableContent = $("tbody");
    tableContent.empty();

    if (data.length == 0) {
        $("table").replaceWith("<h4 class = 'center-text'>No donations yet!</h4>")
        $("#donation-total").empty();
    } else {
        let totalDonation = 0;
        for (let i = 0; i < data.length; i++) {
            let programName = '<td class = "td-inside"><h6>' + data[i].program + '</td></h6>';
            let date = data[i].date_of_donation.split("T")[0] + " " + data[i].date_of_donation.split("T")[1].split(".")[0];
            let donationDate = '<td class = "td-inside"><h6>' + date + '</td></h6>';
            let donationAmount = '<td class = "td-inside"><h6>' + data[i].donation + '</td></h6>';
            let removeButton = '<td><button type="button" value = "inactive" onclick = "changeState(this)" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#donate-removal" id =' + data[i].id + '>X</button></h6>';

            totalDonation += parseInt(data[i].donation);
            tableContent.append('<tr>' + programName + donationDate + donationAmount + removeButton + '</tr>');
        }

        $("#donation-total").empty()
        $("#donation-total").append("Total Donations (Rp) : " + totalDonation);
    }
}

function changeState(element) {
    const val = $(element).attr('value');
    
    if (val === 'inactive') {
        $(element).attr('value', 'active');
    } else {
        $(element).attr('value', 'inactive');
    }
}

function removeDonation() {
   const donationId =  $("[value = 'active']").attr("id");
   const token = $('input[name="csrfmiddlewaretoken"]').attr('value');

   $.ajax({
        method: 'POST',
        url:('https://tp-ppw-crowdfunding.herokuapp.com/kitasabi/delete_donation/'),
        data : JSON.stringify({'id' : donationId,}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function(response) {
            buildTable(response.donations);
        }
    })
}