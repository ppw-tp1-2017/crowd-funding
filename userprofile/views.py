from django.shortcuts import render, redirect
from forms.models import User, Donation
from django.http import JsonResponse
from django.core import serializers
from .models import *
import json

def userprofile(req):
    if req.is_ajax():
        is_auth = False
        if req.user.is_authenticated:
            is_auth = True;
            donations = User.objects.get(email = req.user.email).donation.values()
           
            return JsonResponse({'donations': list(donations), 'is_auth' : is_auth})

        else:   
            return JsonResponse({'is_auth' : is_auth})

    else:
        return render(req, 'userprofile/userprofile.html')

def del_donation(req):
    if req.is_ajax():
        current_user_email = req.user.email
        req = json.loads(req.body)

        Donation.objects.get(id = req['id']).delete();

        donations = User.objects.get(email = current_user_email).donation.values()
           
        return JsonResponse({'donations': list(donations)})

def index(req):
    return redirect('/')


