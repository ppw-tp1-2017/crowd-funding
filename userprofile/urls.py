from django.urls import path
from .views import *

app_name = 'userprofile'

urlpatterns = [
    path('userprofile/', userprofile, name = 'userprofile'),
    path('delete_donation/', del_donation, name = 'del_donation')
]