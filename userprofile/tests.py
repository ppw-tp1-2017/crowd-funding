from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from django.http import HttpRequest

class UserSessionReplica():
    is_authenticated = True

    def __init__(self):
        pass

class UserProfileUnitTest(TestCase):
    def test_url_is_exist(self):

        response = Client().get('/kitasabi/userprofile/')
        self.assertEqual(response.status_code, 200)

    def test_using_userprofile_func(self):
        found = resolve('/kitasabi/userprofile/')
        self.assertEqual(found.func, userprofile)

    def test_template_used(self):
        
        response = Client().get('/kitasabi/userprofile/')
        self.assertTemplateUsed(response, 'userprofile/userprofile.html')
