from django.urls import path
from .views import *
from . import views
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static


app_name = 'news'

urlpatterns = [
    path('news/', news),
]
