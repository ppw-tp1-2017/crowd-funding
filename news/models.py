from django.db import models
from django.utils import timezone
# Create your models here.
class Berita(models.Model):
    judul = models.CharField(max_length = 300)
    isi = models.CharField(max_length = 2000)
    waktu = models.DateTimeField(default = timezone.now)
    image = models.CharField(max_length = 500)
