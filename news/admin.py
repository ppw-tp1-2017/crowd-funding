from django.contrib import admin
from .models import *

# Register your models here.
class ProductBerita(admin.ModelAdmin):
    list_display = ('judul', 'isi', 'waktu', 'image')

admin.site.register(Berita, ProductBerita)
