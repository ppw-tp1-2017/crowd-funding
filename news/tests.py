from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from . import views
# Create your tests here.

class NewsUnitTest(TestCase):
    def test_url_news_exist(self):
        response = Client().get('/kitasabi/news/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_found(self):
        response = Client().get('/not_exist')
        self.assertEqual(response.status_code, 404)

    def test_using_news_func(self):
        found = resolve('/kitasabi/news/')
        self.assertEqual(found.func, views.news)

    def test_news_html(self):
        response = Client().get('/kitasabi/news/')
        self.assertTemplateUsed(response, 'news.html')
