from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import *
from .models import *

def index(req):
    return redirect('/')

def signup(req):
    if req.method == 'POST':
        form = SignUpForm(req.POST)
        if form.is_valid():
            new_user = User()
            new_user.name = form.cleaned_data['name']
            new_user.birthdate = form.cleaned_data['birthdate']
            new_user.email = form.cleaned_data['email']
            new_user.password = form.cleaned_data['password']
            new_user.save()
        return redirect('forms:signup_after')
    else:
        form = SignUpForm()
        return render(req, 'forms/signup.html', {'form': form})

def donate(req):
    if req.method == 'POST':
        form = DonationForm(req.POST)
        if form.is_valid():
            new_donation = Donation()
            new_donation.donor = req.user.first_name + " " + req.user.last_name
            new_donation.anon = form.cleaned_data['anon']
            new_donation.email = req.user.email
            new_donation.donation = form.cleaned_data['donation']
            new_donation.program = form.cleaned_data['program']
            new_donation.save()

            current_user = User.objects.get(email = req.user.email)

            current_user.donation.add(new_donation)

            return redirect('forms:donate_after')
        else:
            return render(req, 'forms/donate.html', {'form': form})
    else:
        form = DonationForm()
        return render(req, 'forms/donate.html', {'form': form})


def signup_after(req):
    return render(req, 'forms/signup_after.html',)

def donate_after(req):
    return render(req, 'forms/donate_after.html',)