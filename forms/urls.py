from django.urls import path
from .views import *

app_name = 'forms'

urlpatterns = [
    path('redirect-home', index, name = 'index'),
    path('signup/', signup, name = 'signup'),
    path('donate/', donate, name = 'donate'),
    path('signup/after/', signup_after, name = 'signup_after'),
    path('donate/after/', donate_after, name = 'donate_after'),
]