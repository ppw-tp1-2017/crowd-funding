# Generated by Django 2.1.1 on 2018-12-13 05:17

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0010_auto_20181213_1212'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donation',
            name='date_of_donation',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 12, 13, 5, 17, 10, 475501, tzinfo=utc)),
        ),
    ]
