from django.test import TestCase, Client
from django.urls import resolve
from .forms import *
from .views import *
from .models import *
import datetime

class SignUpFormUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/kitasabi/signup/')
        self.assertEqual(response.status_code, 200)

    def test_using_signup_func(self):
        found = resolve('/kitasabi/signup/')
        self.assertEqual(found.func, signup)

    def test_using_proper_template(self):
        response = Client().get('/kitasabi/signup/')
        self.assertTemplateUsed(response, 'forms/signup.html')

    def test_signup_returns_proper_responses(self):
        response = Client().get('/kitasabi/signup/')
        self.assertIn('form', response.context)

    def test_signup_can_save_POST_req(self):
        signup_data = { 'name':'NAME',
                        'birthdate': '10/25/2006',
                        'email': 'josh.sudung07@gmail.com',
                        'password':'12345' }

        response = Client().post('/kitasabi/signup/', signup_data)
        obj_count = User.objects.all().count()
        self.assertEqual(obj_count, 1)

        first_obj = User.objects.first()
        self.assertEqual(str(first_obj), 'NAME')
        

class DonationFormUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/kitasabi/donate/')
        self.assertEqual(response.status_code, 200)

    def test_using_donate_func(self):
        found = resolve('/kitasabi/donate/')
        self.assertEqual(found.func, donate)

    def test_using_proper_template(self):
        response = Client().get('/kitasabi/donate/')
        self.assertTemplateUsed(response, 'forms/donate.html')

    def test_donate_returns_proper_responses(self):
        response = Client().get('/kitasabi/donate/')
        self.assertIn('form', response.context)

class AfterFormUnitTest(TestCase):
    def test_signup_after_url_is_exist(self):
        response = Client().get('/kitasabi/signup/after/')
        self.assertEqual(response.status_code, 200)

    def test_using_signup_after_func(self):
        found = resolve('/kitasabi/signup/after/')
        self.assertEqual(found.func, signup_after)

    def test_using_proper_signup_after_template(self):
        response = Client().get('/kitasabi/signup/after/')
        self.assertTemplateUsed(response, 'forms/signup_after.html')

    def test_donate_after_url_is_exist(self):
        response = Client().get('/kitasabi/donate/after/')
        self.assertEqual(response.status_code, 200)

    def test_using_donate_after_func(self):
        found = resolve('/kitasabi/donate/after/')
        self.assertEqual(found.func, donate_after)

    def test_using_proper_donate_after_template(self):
        response = Client().get('/kitasabi/donate/after/')
        self.assertTemplateUsed(response, 'forms/donate_after.html')

class RedirectHomeUnitTest(TestCase):
    def test_redirect_home_url_is_exist(self):
        response = Client().get('/kitasabi/redirect-home')
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/kitasabi/redirect-home')
        self.assertEqual(found.func, index)

class FormModelsUnitTest(TestCase):
    def test_add_obj_to_user_model(self):
        User.objects.create()

        obj_count = User.objects.all().count()
        self.assertEqual(obj_count, 1)

    def test_add_obj_to_donation_model(self):
        Donation.objects.create()

        obj_count = Donation.objects.all().count()
        self.assertEqual(obj_count, 1)

class FormsUnitTest(TestCase):
    def test_signup_form_valid_to_model(self):
        signup_data = { 'name':'NAME',
                        'birthdate': '10/25/2006',
                        'email': 'josh.sudung07@gmail.com',
                        'password':'12345' }
        signup_form = SignUpForm(signup_data)
        self.assertTrue(signup_form.is_valid())

        user_obj = User()
        user_obj.name = signup_form.cleaned_data['name']
        user_obj.save()
        self.assertEqual(str(user_obj), 'NAME')
        self.assertEqual(User.objects.all().count(), 1)