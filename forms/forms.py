from django import forms
from .models import Donation
from .models import User
from django.core.exceptions import ValidationError
import datetime

class SignUpForm(forms.Form):
    name = forms.CharField(label = "name", max_length = 50, widget = forms.TextInput(
        attrs = {
            'class': 'form-attr',
            'placeholder': 'your name',
            'required': True,
        }
    ))

    birthdate = forms.DateField(label = "birth date", widget = forms.DateInput(
        attrs = {
            'class': 'form-attr',
            'required': True,
            'placeholder': 'mm/dd/yyyy'
        }
    ))

    email = forms.EmailField(label = "e-mail", widget = forms.EmailInput(
        attrs = {
            'class': 'form-attr',
            'required': True,
            'placeholder': 'name@email.com',
        }
    ))

    password = forms.CharField(label = "password", widget = forms.PasswordInput(
        attrs = {
            'class': 'form-attr',
            'required': True,
            'placeholder': 'password',
        }
    ))

class DonationForm(forms.Form):
    donor = forms.CharField(label = "name", required = False, max_length = 50, widget = forms.TextInput(
        attrs = {
            'class': 'form-attr',
            'placeholder': 'your name',
            'required': False,
        }
    ))

    email = forms.EmailField(label = "e-mail", required = False, widget = forms.EmailInput(
        attrs = {
            'class': 'form-attr',
            'required': False,
            'placeholder': 'name@email.com',
        }
    ))

    program = forms.ChoiceField(required = False, label = "program", choices = Donation.PROGRAM_CHOICES, widget = forms.Select(
        attrs = {
            'class': 'form-attr',
            'required': False,
            'placeholder': 'choose a program',
        }
    ))

    donation = forms.IntegerField(label = "amount", widget = forms.NumberInput(
        attrs = {
            'class': 'form-attr',
            'required': True,
            'placeholder': 'amount to donate',
        }
    ))

    anon = forms.BooleanField(label = "", required = False, widget = forms.CheckboxInput(
        attrs = {
            'class': 'styled-checkbox', 
            'required': False,}
    ))

    def clean_email(self):
        email = self.cleaned_data['email']

        if not User.objects.filter(email = email).first():
            raise forms.ValidationError('Email anda belum terdaftar. Silahkan login terlebih dahulu.')
        return email