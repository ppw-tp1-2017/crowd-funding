from django.db import models
from programs.models import Program
import datetime
from django.utils.timezone import now

class Donation(models.Model):
    program_set = Program.objects.all()
    PROGRAM_CHOICES = ()

    for prog in program_set:
        new_prog = (prog, prog.title)
        PROGRAM_CHOICES = PROGRAM_CHOICES + (new_prog,)

    donor = models.CharField(max_length = 50)
    anon = models.BooleanField(default = False)
    donation = models.IntegerField(default = 0)
    email = models.EmailField(max_length = 50, default = "None")
    program = models.CharField(max_length = 300, default = "None")
    date_of_donation = models.DateTimeField(default = now, blank = True)

    def __str__(self):
        if self.anon or not self.donor:
            return 'Anonymous'
        return self.donor

class User(models.Model):
    name = models.CharField(max_length = 50)
    birthdate = models.DateField(default = datetime.date.today)
    email = models.EmailField(max_length = 50)
    password = models.CharField(max_length = 30)
    donation = models.ManyToManyField(Donation)

    def __str__(self):
        return self.name