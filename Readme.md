# TP 1 PPW - Crowd Funding - KitaSabi

Tugas Pemrograman 1 PPW 2018  
Kelompok 7 PPW B

## Anggota Kelompok
Mohammad Hasan Albar - 1706025056  
Josh Sudung Pangihutan - 1706039824  
Muhammad Adipashya Yarzuq - 1706074852  
Nathasya Eliora Kristianti - 1706979404 

## Status Pipeline
[![pipeline status](https://gitlab.com/ppw-tp1-2017/crowd-funding/badges/master/pipeline.svg)](https://gitlab.com/ppw-tp1-2017/crowd-funding/commits/master)

## Code Coverage
[![coverage report](https://gitlab.com/ppw-tp1-2017/crowd-funding/badges/master/coverage.svg)](https://gitlab.com/ppw-tp1-2017/crowd-funding/commits/master)

### Link Herokuapp
https://tp-ppw-crowdfunding.herokuapp.com/